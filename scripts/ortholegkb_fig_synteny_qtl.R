# IMPORT
library(neo4r)
library(ggplot2)
library(tidyverse)
library(reshape2)
library(ggrepel)
library(ggstance)
source("scripts/neo4j_functions.R")

# NOTE
# the script queries the database using the neo4r library. To allow to run the
# script without any running database, functions querying the database were
# commented and resulting data was saved as csv, and are read in the script


# FUNCTIONS
db = neo4j_db_connection(url = "http://0.0.0.0:7474/",
                         user = "neo4j",
                         pwd = "test")

## query wrapper
cypher = function(query, neo4j = ortholegkb, ...) call_neo4j(
  query = query, con = neo4j, ...)

get_overlapping_blocks = function(sb_anchor, goi){
  # from an anchor syntenic block, get connected blocks
  genome_1 = goi %>% get_cypher_list()
  genome_2 = genome_1
  q = sprintf(
    'MATCH (sb1:Synteny)-[s1:LOCATED_ON]->(c1:Chromosome)-[:SUBSET_OF]->(g1:Genome)
  MATCH (c1)<-[s2:LOCATED_ON]-(sb2:Synteny)-->(c2:Chromosome)-[:SUBSET_OF]->(g2:Genome)
  WHERE
    sb1.block_id = "%s" AND
    g1.genome_id in [%s] AND
    g2.genome_id in [%s] AND
    ((s2.start >= s1.start AND s2.end <= s1.end) OR
    (s2.start <= s1.start AND s2.end >= s1.end) OR
    (s1.start <= s2.start <= s1.end AND s2.end >= s1.end) OR
    (s2.start <= s1.start <= s2.end AND s1.end >= s2.end))
    WITH COLLECT(DISTINCT sb2) as found_blocks
    UNWIND found_blocks as sb2
  MATCH (c1:Chromosome)<-[s1:LOCATED_ON]-(sb2)-[s2:LOCATED_ON]->(c2:Chromosome)
    WHERE id(c1) > id(c2)
  RETURN sb2.block_id as block_id,
  c1.chromosome_id as chrA, c2.chromosome_id as chrB, 
  s1.start as sA_start, s1.end as sA_end,
  s2.start as sB_start, s2.end as sB_end', 
    sb_anchor, genome_1, genome_2) %>% cypher(db, type = "row")
  df = query_to_tibble(q)
  return(df)
}

get_synteny_longer_format = function(df){
  # get a longer format (chrA and chrB as chr)
  df_chrA = df %>% select(block_id, chr = chrA, start = sA_start, end = sA_end,
                          block_color)
  df_chrB = df %>% select(block_id, chr = chrB, start = sB_start, end = sB_end,
                          block_color)
  df_m = rbind(df_chrA, df_chrB)
  return(df_m)
}

reorder_blocks_coordinates = function(df, genes = TRUE){
  # the geom_polygon function needs to get coordinates in a specific order
  # otherwise the fill of the polygon will flip in the middle
  # doing 2--3
  #       1--4
  # needs a synteny df in longer format
  df_chr_id = df %>% select(chr) %>% unique() %>%
    rownames_to_column(var = "chr_id")
  df_chr_id$chr_id = as.numeric(df_chr_id$chr_id)
  # get chr info on df
  df_chr_order = df %>% select(block_id, chr, start, end, block_color) %>%
    melt(id.vars = c("block_id", "chr", "block_color")) %>% arrange(block_id, chr) %>% 
    left_join(., df_chr_id, by = "chr")
  if(genes){
    df_chr_order = df %>% select(block_id, chr, start, end,
                                 gene_id, g_start, g_stop, width, block_color) %>%
      melt(id.vars = c("block_id", "chr", "gene_id", "block_color")) %>% 
      arrange(block_id, chr) %>% left_join(., df_chr_id, by = "chr")
  }
  # classic order for start (1 and 2)
  df_start = df_chr_order %>% filter(variable == "start") %>% 
    arrange(block_id, chr_id)
  # reversed order for end (3 then 4)
  df_end = df_chr_order %>% filter(variable == "end") %>% 
    arrange(block_id, -chr_id)
  df_m = rbind(df_start, df_end) %>% as_tibble()
  if(genes){
    df_genes = df_chr_order %>% 
      filter(variable %in% c("g_start", "g_stop")) %>% as_tibble()
    df_m = df_m %>% bind_rows(df_genes)
  }
  return(df_m)
}

filter_allowed_chr = function(df, v_chr){
  # to avoid spreading on many chromosomes, restrict
  # to those mentionned in v_chr
  df_filtered = df %>% filter(chrA %in% v_chr)
  df_filtered = df_filtered %>% filter(chrB %in% v_chr)
  return(df_filtered)
}

collect_all_blocks = function(df, goi){

  v_blocks = df %>% select(block_id) %>% unique() %>% pull()
  v_chr = c(df$chrA, df$chrB) %>% unique()
  
  # df to fill
  df_all_blocks = data.frame(row.names = c("block_id", "chrA", "chrB",
                                           "sA_start", "sA_end",
                                           "sB_start", "sB_end"))
  for (i in v_blocks){
    # TODO doing it for the initial block, which is useless
    df_all_blocks = rbind(df_all_blocks, get_overlapping_blocks(i, goi))
  }
  df_all_synt_blocks = df_all_blocks %>% unique()
  df_all_synt_blocks_filtered = filter_allowed_chr(df_all_synt_blocks, v_chr)
  return(df_all_synt_blocks_filtered)
}

get_genes_in_blocks = function(df, goi, genes_of_interest = NULL){
  # from syntenic blocks, collect genes connected to them (PART_OF)
  v_blocks = df %>% select(block_id) %>% unique() %>% pull() %>%
    get_cypher_list()
  genomes = goi %>% get_cypher_list()
  if(!is.null(genes_of_interest)){
    genes = genes_of_interest %>% get_cypher_list()
    query = sprintf(
      'MATCH (sb:Synteny)<-[:PART_OF]-(g:Gene)
  WHERE sb.block_id in [%s]
  AND g.genome in [%s]
  AND g.gene_id in [%s]
  RETURN sb.block_id as block_id,
  g.gene_id as gene_id, g.chromosome as chr,
  g.start as g_start, g.stop as g_stop, g.stop-g.start as width',
      v_blocks, genomes, genes)
  } else {
    query = sprintf(
      'MATCH (sb:Synteny)<-[:PART_OF]-(g:Gene)
  WHERE sb.block_id in [%s]
  AND g.genome in [%s]
  RETURN sb.block_id as block_id,
  g.gene_id as gene_id, g.chromosome as chr,
  g.start as g_start, g.stop as g_stop, g.stop-g.start as width',
      v_blocks, genomes)
  }
  q = query %>% cypher(db, type = "row")
  df_genes = query_to_tibble(q)
  df_merged = df %>% left_join(., df_genes, by = c("block_id", "chr"),
                               multiple = "all")
  return(df_merged)
}
assign_block_colors = function(df){
  # to get colors by chromosome pairs (will not really reduce the number
  #  of colors, since expecting 1 block per chr on average)
  df_chr_pair_colors = df %>% select(chrA, chrB) %>%
    distinct(smaller = pmin(chrA, chrB),
             larger = pmax(chrA, chrB),
             .keep_all = TRUE) %>%
    select(-smaller, -larger)
  df_chr_pair_colors$block_color = row.names(df_chr_pair_colors)
  df_colors = df %>% left_join(., df_chr_pair_colors, by = c("chrA", "chrB"))
  return(df_colors)
}

get_overlapping_qtl = function(qtl_anchor, goi){
  # from an anchor syntenic block, get connected blocks
  genome_1 = goi %>% get_cypher_list()
  genome_2 = genome_1
  q = sprintf(
    'MATCH (q1:QTL)<--(:Gene)-->(sb1:Synteny)-[s1:LOCATED_ON]->(c1:Chromosome)-[:SUBSET_OF]->(g1:Genome)
  MATCH (c1)<-[s2:LOCATED_ON]-(sb2:Synteny)-->(c2:Chromosome)-[:SUBSET_OF]->(g2:Genome)
  WHERE
    q1.qtl_id = "%s" AND
    g1.genome_id in [%s] AND
    g2.genome_id in [%s] AND
    ((s2.start >= s1.start AND s2.end <= s1.end) OR
    (s2.start <= s1.start AND s2.end >= s1.end) OR
    (s1.start <= s2.start <= s1.end AND s2.end >= s1.end) OR
    (s2.start <= s1.start <= s2.end AND s1.end >= s2.end))
    WITH COLLECT(DISTINCT sb2) as found_blocks
    UNWIND found_blocks as sb2
MATCH (c1:Chromosome)<-[s1:LOCATED_ON]-(sb2)-[s2:LOCATED_ON]->(c2:Chromosome)
    WHERE id(c1) > id(c2)
WITH sb2 as common_blocks
MATCH (c:Chromosome)<-[s:LOCATED_ON]-(q2:QTL)<--(:Gene)-->(common_blocks),
        (t:Trait)<--(q2)
  WHERE t.trait_id =~ ".*flower.*"
  RETURN DISTINCT q2.qtl_id as qtl_id,
  c.chromosome_id as chr, s.start as start, s.end as end', 
    qtl_anchor, genome_1, genome_2) %>% cypher(db, type = "row")
  df = query_to_tibble(q)
  return(df)
}

# MAIN
# genome of interest
goi = c("mtrun", "psat", "lcul", "vfab")
genes_of_interest = c("MtrunA17Chr7_39606925_39618489",
                      "Vfaba.Hedin2.R1.5g087000", "Vfaba.Hedin2.R1.1g183400",
                      "Lcu.2RBY.6g043850", "Lcu.2RBY.2g046590",
                      "Psat0s4214g0120", "Psat3g090680", "Psat3g090720",
                      "Psat1g096760", "MtrunA17Chr6g0461711", "Vradi09g00002316",
                      "Psat3g090720", "Psat3g090680/Psat3g090720")
# the mcscanx_block_671 between is the anchor syntenic block
sb_anchor = "mcscanx_block_671"

# obtain blocks overlapping this anchor
## df_olp_anchor = get_overlapping_blocks(sb_anchor, goi = goi)
df_olp_anchor = read_csv("data/fta1_usecase/df_syntenic_blocks.csv")
## df_olp = collect_all_blocks(df_olp_anchor, goi = goi)
df_olp = read_csv("data/fta1_usecase/df_syntenic_blocks_full.csv")
df_olp_colored = assign_block_colors(df_olp)
df_olp_m = get_synteny_longer_format(df_olp_colored)
## df_blocks_genes = get_genes_in_blocks(df_olp_m, goi, genes_of_interest)
df_blocks_genes = read_csv("data/fta1_usecase/df_sb_genes.csv")
df_blocks_genes_ordered = reorder_blocks_coordinates(df_blocks_genes,
                                                     genes = T)

# adding psat genes that do not belong to syntenic blocks (for display)
df_blocks_genes_ordered = rbind(df_blocks_genes_ordered,
                                data.frame(block_id = c("x"),
                                chr = c("Ps03"),
                                gene_id = c("Psat3g090680/Psat3g090720"),
                                block_color = c(1),
                                variable = c("g_start"),
                                value = c(186528296),
                                chr_id = c(1)))


########################### SYNTENIC BLOCKS VISU ###############################
ggplot(df_blocks_genes_ordered) +
  geom_polygon(data = df_blocks_genes_ordered %>%
                 filter(variable %in% c("start", "end")),
               aes(x = value, y = as.factor(chr),
                   group = block_id, fill = block_color), alpha = 0.4) +
  geom_line(data = df_blocks_genes_ordered %>%
              filter(variable %in% c("start", "end")) %>% 
              select(-block_id, -block_color, -chr_id, -gene_id) %>% unique(),
            aes(x = value, y = as.factor(chr)), color = "black", lwd = 0.6) +
  geom_point(data = df_blocks_genes_ordered %>% 
               filter(gene_id %in% genes_of_interest & 
                        variable %in% c("g_start")) %>% 
               select(-block_id, -block_color, -chr_id) %>% unique(),
             aes(x = value, y = as.factor(chr)), color = "black", size = 1.5) +
  geom_text(data = df_blocks_genes_ordered %>%
              filter(gene_id %in% genes_of_interest &
                       variable %in% c("g_start")) %>% 
              select(-block_id, -block_color, -chr_id) %>% unique(),
            aes(x = value, y = as.factor(chr), label = gene_id), stat = "identity",
            nudge_y = 0.1, vjust = "inward", hjust = "inward", size = 3.5) +
  scale_x_continuous(breaks = scales::pretty_breaks(n = 10),
                     labels = function(x) format(x, scientific = FALSE)) +
  ylab(label = "chromosome") +
  xlab(label = "position (bp)") +
  theme_classic() +
  theme(legend.position = "none") +
  scale_fill_brewer(palette = "Dark2")

################### QTL IN SYNTENIC BLOCKS #####################################
## df_olp_qtl = get_overlapping_qtl("qDTF.6-2_1", goi = goi)
df_olp_qtl = read_csv("data/fta1_usecase/df_ft_qtl.csv")
df_olp_qtl$middle = (df_olp_qtl$end + df_olp_qtl$start)/2

df_olp_qtl_melt = melt(df_olp_qtl)

ggplot(df_blocks_genes_ordered) +
  geom_hline(aes(yintercept = as.factor(chr)), color = "black") +
  geom_polygon(data = df_blocks_genes_ordered %>%
                 filter(variable %in% c("start", "end")),
               aes(x = value, y = as.factor(chr),
                   group = block_id, fill = block_color), alpha = 0.4) +
  geom_line(data = df_olp_qtl_melt %>% filter(variable != "middle"),
            aes(x = value, y = as.factor(chr)), color = "black", lwd = 1.5) +
  geom_point(data = df_olp_qtl_melt %>% filter(variable == "middle"),
             aes(x = value, y = as.factor(chr)), fill = "black") +
  geom_label_repel(data = df_olp_qtl_melt %>% 
                     filter(variable == "middle") %>% unique(),
                   aes(x = value, y = as.factor(chr), label = qtl_id), 
                   fill = "lightgray", label.size = 0.4, nudge_x = 0.1, nudge_y = 0.38) +
  geom_point(data = df_blocks_genes_ordered %>% 
               filter(gene_id %in% genes_of_interest & 
                        variable %in% c("g_start")) %>% 
               select(-block_id, -block_color, -chr_id) %>% unique(),
             aes(x = value, y = as.factor(chr)), shape = "circle",
             color = "red", size = 2) +
  scale_x_continuous(breaks = scales::pretty_breaks(n = 10),
                     labels = function(x) format(x, scientific = FALSE)) +
  ylab(label = "chromosome") +
  xlab(label = "position (bp)") +
  theme_classic() +
  theme(legend.position = "none") +
  scale_fill_brewer(palette = "Dark2")
